package sqlconnmanager

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"time"
)

var (
	_ DBer  = (*sql.DB)(nil)
	_ PQEr  = (*sql.DB)(nil)
	_ PQETr = (*sql.Tx)(nil)
)

type Queryer interface {
	Query(query string, args ...any) (*sql.Rows, error)
	QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
	QueryRow(query string, args ...any) *sql.Row
	QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
}

type Preparer interface {
	Prepare(query string) (*sql.Stmt, error)
	PrepareContext(ctx context.Context, query string) (*sql.Stmt, error)
}

type Execer interface {
	Exec(query string, args ...any) (sql.Result, error)
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
}

type PQEr interface {
	Queryer
	Preparer
	Execer
}

type Txer interface {
	Rollback() error
	Stmt(stmt *sql.Stmt) *sql.Stmt
	StmtContext(ctx context.Context, stmt *sql.Stmt) *sql.Stmt
	Commit() error
}

type PQETr interface {
	Queryer
	Preparer
	Execer
	Txer
}

type DBer interface {
	PQEr

	BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error)

	Close() error
	Conn(ctx context.Context) (*sql.Conn, error)
	Driver() driver.Driver

	PingContext(ctx context.Context) error
	Ping() error

	SetConnMaxIdleTime(d time.Duration)
	SetConnMaxLifetime(d time.Duration)
	SetMaxIdleConns(n int)
	SetMaxOpenConns(n int)
	Stats() sql.DBStats
}
