# Common SQL connection manager

Обобщенный менеджер соединений для SQL на базе `database/sql`.

Менеджер реализует интерфейс:

```golang
type IConnectManager interface {
	Conn() DBer

	Builder() sq.StatementBuilderType

	Begin(context.Context, *sql.TxOptions) (PQETr, error)
	Done(context.Context) error

	WithTxFuncs(context.Context, bool, ...func(context.Context, PQETr) error) error
	WithFunc(context.Context, func(context.Context, PQEr) error) error
}
```

## Оглавление

1. [Установка](#установка)
2. [Зависимости](#зависимости)
3. [Список задач](#список-задач)
4. [Лицензия](#лицензия)

## Установка

Установка из исходников:

```shell
go get gitlab.com/ovsinc/sql-conn-manager
```

## Зависимости

- `database/sql`
- `github.com/ovsinc/multilog`
- `github.com/google/uuid`

## Список задач

- [x] Использовать интеерфейсы вместо `*sql.Tx`, `*sql.DB`.
- [ ] Описание управления транзакцией.

## Лицензия

Apache License 2.0
