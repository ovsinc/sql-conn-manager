package sqlconnmanager

import (
	"context"

	"github.com/google/uuid"
)

type reqTxID struct{}

func NewRequestContext(ctx context.Context) context.Context {
	newCtx := context.WithValue(ctx, reqTxID{}, uuid.NewString())
	return newCtx
}

func GetRequestID(ctx context.Context) string {
	id, _ := ctx.Value(reqTxID{}).(string)
	return id
}
