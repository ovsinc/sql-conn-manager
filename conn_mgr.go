package sqlconnmanager

import (
	"context"
	"database/sql"
	"errors"
	"sync"

	sq "github.com/Masterminds/squirrel"
	logger_pkg "github.com/ovsinc/multilog"
)

var ErrNoTx = errors.New("no *sql.Tx in Context")

type IConnectManager interface {
	Conn() DBer

	Builder() sq.StatementBuilderType

	Begin(context.Context, *sql.TxOptions) (PQETr, error)
	Done(context.Context) error

	WithTxFuncs(context.Context, bool, ...func(context.Context, PQETr) error) error
	WithFunc(context.Context, func(context.Context, PQEr) error) error
}

func NewSQLConnManager(
	db DBer,
	logger logger_pkg.Logger,
	builder sq.StatementBuilderType,
) IConnectManager {
	return &sqliteManager{
		logger:  logger,
		db:      db,
		builder: builder,
		txs:     make(map[string]PQETr),
		mu:      new(sync.Mutex),
	}
}

type sqliteManager struct {
	db      DBer
	logger  logger_pkg.Logger
	builder sq.StatementBuilderType

	txs map[string]PQETr
	mu  *sync.Mutex
}

func (x *sqliteManager) Conn() DBer {
	return x.db
}

func (x *sqliteManager) Builder() sq.StatementBuilderType {
	return x.builder
}

func (x *sqliteManager) WithFunc(ctx context.Context, fn func(context.Context, PQEr) error) error {
	return fn(ctx, x.db)
}

func (x *sqliteManager) Begin(ctx context.Context, ops *sql.TxOptions) (PQETr, error) {
	_, tx, err := x.getTx(ctx, true, ops)
	return tx, err
}

func (x *sqliteManager) delete(id string) {
	x.mu.Lock()
	defer x.mu.Unlock()
	delete(x.txs, id)
}

func (x *sqliteManager) done(id string, tx PQETr) error {
	errCommit := tx.Commit()
	if errCommit != nil {
		if errRollback := tx.Rollback(); errRollback != nil {
			x.logger.Warnf("sqliteManager.done SQL rollback failed: %v", errRollback)
		}
	}

	x.delete(id)

	return errCommit
}

func (x *sqliteManager) Done(ctx context.Context) error {
	id, tx, err := x.getTx(ctx, false, nil)
	if err != nil {
		return err
	}

	return x.done(id, tx)
}

func (x *sqliteManager) WithTxFuncs(
	ctx context.Context,
	done bool,
	fns ...func(context.Context, PQETr) error,
) error {
	id, tx, err := x.getTx(ctx, true, nil)
	if err != nil {
		return err
	}

	defer func() {
		if done {
			x.delete(id)
		}
	}()

	for _, fn := range fns {
		if errFn := fn(ctx, tx); errFn != nil {
			if errRollback := tx.Rollback(); errRollback != nil {
				x.logger.Warnf("sqliteManager.WithTxFunc tx.Rollback fails: %v", errRollback)
			}
			return errFn
		}
	}

	if done {
		return x.done(id, tx)
	}

	return nil
}

func (x *sqliteManager) getTx(ctx context.Context, mustNew bool, ops *sql.TxOptions) (string, PQETr, error) {
	reqID := GetRequestID(ctx)

	x.mu.Lock()
	defer x.mu.Unlock()

	if v, ok := x.txs[reqID]; ok {
		return reqID, v, nil
	}

	if !mustNew {
		return "", nil, ErrNoTx
	}

	tx, err := x.db.BeginTx(ctx, ops)
	if err != nil {
		return "", nil, err
	}

	x.txs[reqID] = tx

	return reqID, tx, nil
}
