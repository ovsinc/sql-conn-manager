package sqlconnmanager

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRequestContext(t *testing.T) {
	ctx := NewRequestContext(context.Background())
	ctxVal := ctx.Value(reqTxID{})

	assert.NotNil(t, ctxVal)

	reqID, ok := ctxVal.(string)
	assert.True(t, ok)
	assert.NotEmpty(t, reqID)
}

func TestGetRequestID(t *testing.T) {
	ctx := NewRequestContext(context.Background())
	reqID, _ := ctx.Value(reqTxID{}).(string)

	assert.Equal(t, reqID, GetRequestID(ctx))
}
