_CURDIR := `git rev-parse --show-toplevel 2>/dev/null | sed -e 's/(//'`


.PHONY:
tests: ## Run all test
	go test -coverprofile=coverage.out ./...

.PHONY:
show_cover: ## Show code covery
	go tool cover -html=coverage.out

.PHONY:
lint: ## Lint the code
	golangci-lint run

.PHONY:
clean_cache: ## Clean build cache
	go clean -cache


## Help and autoindent
.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
