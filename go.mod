module gitlab.com/ovsinc/sql-conn-manager

go 1.19

require (
	github.com/Masterminds/squirrel v1.5.3
	github.com/google/uuid v1.3.0
	github.com/ovsinc/multilog v1.0.6
	github.com/stretchr/testify v1.8.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
